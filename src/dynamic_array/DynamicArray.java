package dynamic_array;

import java.util.Iterator;

@SuppressWarnings("unchecked")
public class DynamicArray<T> implements Iterable<T> {
    private T[] arr;
    private int length = 0;
    private int capacity = 0;

    public DynamicArray() {
        this(16);
    }

    public DynamicArray(int capacity) {
        if (capacity < 0) throw new IllegalArgumentException("Illegal Capacity: " + capacity);
        this.capacity = capacity;
        arr = (T[]) new Object[capacity];
    }

    public int size() {
        return length;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public T get(int index) {
        return arr[index];
    }

    public void set(int index, T element) {
        arr[index] = element;
    }

    public void clear() {
        for (int i = 0; i < capacity; i++) {
            arr[i] = null;
        }
        length = 0;
    }

    public void add(T element) {

        // if current capacity is less for our data, we will resize it.
        if (length + 1 >= capacity) {
            if (capacity == 0) capacity = 1;
            else capacity *= 2;

            T[] new_array = (T[]) new Object[capacity];
            for (int i = 0; i < length; i++) {
                new_array[i] = arr[i];
            }

            arr = new_array;
        }

        arr[length++] = element;
    }


    public boolean remove(Object object) {
        for (int i = 0; i < length; i++) {
            if (arr[i].equals(object)) {
                removeAt(i);
                return true;
            }
        }

        return false;
    }

    public T removeAt(int index) {
        if (index >= length || index < 0) throw new IndexOutOfBoundsException();
        T data = arr[index];

        T[] new_array = (T[]) new Object[length - 1];
        for (int i = 0, j = 0; i < length; i++, j++) {
            if (i == index) j--;
            else new_array[j] = arr[i];
        }

        arr = new_array;
        capacity = --length;

        return data;
    }

    public int indexOf(Object object) {
        for (int i = 0; i < length; i++) {
            if (arr[i].equals(object)) {
                return i;
            }
        }

        return -1;
    }

    public boolean contains(Object object) {
        return indexOf(object) != -1;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < length;
            }

            @Override
            public T next() {
                return arr[index++];
            }
        };
    }

    @Override
    public String toString() {
        if (length == 0) return "[]";
        else {
            StringBuilder stringBuilder = new StringBuilder(length).append("[");
            for (int i = 0; i < length - 1; i++) {
                stringBuilder.append(arr[i] + ", ");
            }

            return stringBuilder.append(arr[length - 1] + "]").toString();
        }
    }
}
